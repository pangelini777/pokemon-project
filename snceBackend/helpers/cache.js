
const {Teams,Pokemons} = require('../models/models')
const redis = require('redis')
const client = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASS
})
const cacheMgmt = function doCache(){
    client.flushdb( function (err, succeeded) {
        console.log("Cache cleared!")
        rebuildCache()
            .then(result => {
                console.log("Cache built!")
            })
            .catch(err => {
                console.log(err)
            })
    });
}


async function rebuildCache(){
    return new Promise((resolve,reject)=>{
        console.log("Building new cache from postgres...")
        new Teams().orderBy('created_at', 'asc').fetchAll({withRelated: ['memberships']}).then((teams) => {
            teams = teams.toJSON()
            teams.forEach(team =>{
                let pokemons = []
                let total_base_experience = 0;
                let toResolve = []
                team.memberships.forEach(membership => {
                    let promise = new Pokemons({id:membership.pokemon_id}).fetch().then(pokemon=>{
                        pokemon = pokemon.toJSON()
                        pokemons.push(pokemon);
                        total_base_experience += pokemon.base_experience;
                    })
                    toResolve.push(promise)
                })
                Promise.all(toResolve)
                    .then(solved=>{
                        let teamToInsert = {
                            id: team.id,
                            name: team.name,
                            pokemons: pokemons,
                            total_base_experience: total_base_experience
                        }
                        client.set(`team_${team.id}`,JSON.stringify(teamToInsert))
                    })
                    .catch(err => {
                        console.log(err)
                    })

            })
            resolve(true)
        }).catch((error) => {
            console.log(error)
            reject(false)
        })
    })
}

module.exports = cacheMgmt