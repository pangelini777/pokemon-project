var express = require('express');
var router = express.Router();
const axios = require('axios').default;
const database = require('../db/db')
const cacheMgmt = require('../helpers/cache')
const {Teams} = require('../models/models')
const redis = require('redis')
const client = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASS
})

router.post('/', (req, res) =>{
    if(req.body && req.body.name){
        const teamName = req.body.name;
        //check if team alredy exists
        database('teams').where({name:teamName})
            .then(existanceCheck =>{
                if(existanceCheck.length > 0) {
                    console.log("Already exists")
                    res.status(400).json({status:400,message:`Team ${teamName} already exists!`})
                }else{
                    database('teams').returning('id').insert({name:teamName})
                        .then(stored =>{
                            res.json({status:200,message:`Team ${teamName} created successfully!`,team:{name:teamName,id:stored[0]}})
                            //caching result
                            const emptyTeam = {
                                "name": teamName,
                                "pokemons": [],
                                "total_base_experience": 0
                            }
                            client.set(`team_${stored[0]}`,JSON.stringify(emptyTeam))
                        })
                        .catch(err => {
                            console.log(err)
                            res.status(400).json({status:400,message:`Team ${teamName} cannot be stored!`})
                        })
                }
            })
    }else{
        res.status(400)
    }
})


router.put('/:team_id([0-9]+)', (req,res) => {
    if(req.body && req.body.newName && req.params.team_id) {
        const newName = req.body.newName;
        const teamId = req.params.team_id;
        database('teams').where({id:req.params.team_id}).update({name:newName,updated_at:database.fn.now()})
            .then(updated => {
                res.json({status:200,message:`The name of the team has been updated to ${newName}`})
                //update cache
                client.get(`team_${teamId}`,(err,team) =>{
                    team = JSON.parse(team)
                    client.set(`team_${teamId}`, JSON.stringify(team))
                })
            })
            .catch(err => {
                res.status(400).json({status:400,message:`Unable to update then name of team!`})
            })
    }
})


router.get('/list', (req,res)=>{
    if(!req.query.fresh){
        //Use cache first
        console.log("Getting cached data")
        client.keys('*', function (err, teams) {
            if (err) return console.log(err);
            if(teams){
                const teamList = []
                const resolveTeams = []
                teams.forEach(team => {
                    let resolveTeam =  new Promise((resolve,reject) =>{
                        client.get(team,(err,result)=>{
                            teamList.push(JSON.parse(result))
                            resolve(result)
                        })
                    })
                    resolveTeams.push(resolveTeam)
                })
                Promise.all(resolveTeams)
                    .then(completed => {

                        res.json(teamList.sort((a, b) => (a.id > b.id) ? 1 : -1))
                    })
                    .catch(err => {
                        res.json(err)
                    })
            }
        })
    }else{
        console.log("Getting fresh data")
        new Teams().fetchAll({withRelated: ['pokemons']})
            .then((teams) => {
                res.json(teams)
                cacheMgmt();
            })
            .catch(err => {
                console.log(err)
                res.json(err)
            })
    }
})

module.exports = router;
