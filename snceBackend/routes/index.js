var express = require('express');
var router = express.Router();
const axios = require('axios').default;
const database = require('../db/db')
const cacheMgmt = require('../helpers/cache')
const redis = require('redis')
const client = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASS
})
router.post('/addPoke/:team_id([0-9]+)', (req, res) =>{
  if(req.params.team_id){
    const teamId = req.params.team_id
    client.get(`team_${teamId}`, (err, team) => {
      team = JSON.parse(team);
      const exclusionList = []
      if(process.env.MULTIPLE_NOT_ALLOWED==="true"){
          console.log("Multiple not allowed constraint active!")
          team.pokemons.forEach(member=>{
              exclusionList.push(member.external_id)
          })
      }
      getRandomPokemon(exclusionList)
          .then(poke=>{

            if(!poke.inPokedex){
                poke.pokemon.abilities = JSON.parse(poke.pokemon.abilities)
                poke.pokemon.types = JSON.parse(poke.pokemon.types)
            }
            database('memberships').insert({team_id:teamId,pokemon_id:poke.pokemon.id})
                .then(() => {
                    res.json({status:200,message:`New pokemon added successfully!`,pokemon:poke})
                    team.pokemons.push(poke.pokemon);
                    team.total_base_experience += poke.pokemon.base_experience;
                    client.set(`team_${teamId}`, JSON.stringify(team));
                })
                .catch(err => {
                    res.status(400)
                })


          })
    })

  }else{
    res.status(400)
  }

})

/* GET home page. */
router.get('/', (req, res, next) => {
  res.json({title: 'Pokemon tournament team creator', version:"0.0.1"});
});

router.get('/rebuildCache', (req,res)=>{
    console.log("Clearing and rebuilding cache...")
    cacheMgmt();
    console.log("Caching complete!")
    res.json({status:200,message:"ok!"})

})

async function getRandomPokemon(exclusionList){
  return new Promise((resolve,reject)=>{
    console.log("Getting random poke")
    let pokeId
    if (!exclusionList.length){
      pokeId = Math.floor(Math.random() * 807) + 1;
    }else{
      console.log("Not Empty!")
      pokeId = exclusionList[0]
      while(exclusionList.includes(pokeId)){
        pokeId = Math.floor(Math.random() * 807) + 1;
      }
    }
    //Testing multiple for cache
    // pokeId = 1
    //check if pokemon is already in db
    database('pokemons').where({external_id:pokeId})
        .then(pokemon=>{
            if(pokemon.length>0){
                //pokemon already in db
                console.log(pokemon[0])
                resolve({pokemon:pokemon[0],inPokedex:true})
            }else{
                //fetch pokemon from api, and add to the db
                try {
                    console.log("Getting pokemon with id: "+pokeId)
                    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokeId}/`)
                        .then(result=>{
                            const pokeInfo = result.data;
                            const toReturn = {
                                external_id: pokeInfo.id,
                                name: pokeInfo.name,
                                types: JSON.stringify(pokeInfo.types),
                                base_experience: pokeInfo.base_experience,
                                abilities: JSON.stringify(pokeInfo.abilities),
                                sprite: pokeInfo.sprites.front_default
                            }

                            database('pokemons').insert(toReturn).returning('id')
                                .then(
                                    function (id){
                                        toReturn.id = id[0];
                                        resolve({pokemon:toReturn,inPokedex:false});
                                    }
                                )
                        })
                } catch (error) {
                    reject("error")
                }
            }
        })
  })

}




module.exports = router;
