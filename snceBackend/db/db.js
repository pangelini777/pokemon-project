const env = process.env.NODE_ENV;
const {development,test} = require('../knexfile')
const knex = require("knex");

let database
if (env === 'test'){
    console.log("TEST")
    database = knex(test);
}else{
    console.log("DEV")
    database = knex(development);
}

module.exports = database;