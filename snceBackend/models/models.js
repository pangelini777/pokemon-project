const database = require('../db/db')
const bookshelf = require('bookshelf')(database)
const Teams = bookshelf.model('Teams', {
    tableName: 'teams',
    memberships() {
        return this.hasMany('Memberships')
    },
    pokemons(){
        return this.belongsToMany(Pokemons, 'memberships')
    }
})
const Pokemons = bookshelf.model('Pokemons', {
    tableName: 'pokemons',
    memberships() {
        return this.hasMany('Memberships')
    }
})

const Memberships = bookshelf.model('Memberships', {
    tableName: 'memberships',
})

module.exports = {Teams,Pokemons,Memberships}