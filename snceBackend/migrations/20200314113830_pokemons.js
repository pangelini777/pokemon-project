
exports.up = function(knex) {
    return knex.schema.createTable('pokemons', function(table) {
        table.increments();
        table.string('name').notNullable().unique();
        table.integer('external_id').notNullable();
        table.integer('base_experience').notNullable();
        table.string('sprite').notNullable();
        table.jsonb('abilities').notNullable();
        table.jsonb('types').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('pokemons');
};
