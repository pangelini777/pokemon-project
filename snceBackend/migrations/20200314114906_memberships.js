
exports.up = function(knex) {
    return knex.schema.createTable('memberships', function(table) {
        table.increments();
        table.integer('team_id').notNullable().references('id').inTable('teams').onDelete('cascade');
        table.integer('pokemon_id').notNullable().references('id').inTable('pokemons');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('memberships');
};