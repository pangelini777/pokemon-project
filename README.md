# Pokemon Tournament Project

## Project setup
The system is preconfigured so in order to start the system run in the project root directory:
```
docker-compose --build -d
```
The Web Application will run on
```
http://localhost:8080
```

## Configuration
The system can be configured using the .env file inside the project root folder. The following options are available:
```
#PORTS
POSTGRES_PORT=5432
REDIS_PORT=6379
FRONTEND_PORT=8080
#POSTGRES
POSTGRES_HOST=pokemon-postgres-server
POSTGRES_USER=postgres
POSTGRES_PASS=sncerocks
#REDIS
REDIS_HOST=pokemon-redis-server
REDIS_PASS=sncerocks
#EXTRA CONFIG
MULTIPLE_NOT_ALLOWED=true
```
Almost everything in this file is self-describing.

MULTIPLE_NOT_ALLOWED is a flag to add a one-pokemon-of-a-kind constraint for each team.
I have added this flag because this requirement was not clear from the Challenge specifications.

## Architecture
The system is composed by:
- NodeJs Backend
- VueJs Frontend
- Nginx Server
- Postgres Database with Persistence (using volumes)
- Redis Database in memory for caching purpose

To be sure that everything spin up in the right order I have added some constraints in the docker-compose.yml file.
 
Moreover I used docker-compose-wait in order to be able to start NodeJs Backend as soon as Postgres and Redis are reachable. 

## Cache system
The application use the Redis Database to cache the Teams with all the related Pokemons.

When the application start the system will rebuild the cache based on the Postgres persisted data.

The cache can be rebuilt programmatically calling the API route:
```
http://{api_url}:{api_port}/rebuildCache
```
Furthermore the teams are "cached" inside the Vuex store, so it will actually call the API to fetch the Teams only for sync purpose.

