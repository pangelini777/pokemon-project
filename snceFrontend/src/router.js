import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import TeamList from './views/TeamList.vue'
import TeamCreate from './views/TeamCreate.vue'
import Team from '@/views/Team'
import TeamEdit from '@/views/TeamEdit';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/team/list',
      name: 'teamList',
      component: TeamList
    },
    {
      path: '/team/create',
      name: 'teamCreate',
      component: TeamCreate
    },
    {
      path: '/team/:id',
      name: 'team',
      component: Team
    },
    {
      path: '/team/:id/edit',
      name: 'teamEdit',
      component: TeamEdit
    }
  ]
})
