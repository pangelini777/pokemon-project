export default {

  appendTeam: (state, { team }) => {
    team.pokemons = []
    state['teams'].push(team)
    return team
  },
  appendPokemon: (state, { team_id,pokemon }) => {
    let teams = state['teams']
    teams.forEach(team=>{
      if(team.id === team_id){
        team.pokemons.unshift(pokemon.pokemon)
      }
    })
    state['teams'] = teams;

  },
  updateFromCache: (state,payload) => {
    state['teams'] = payload.data;
  },
  setUpdateStatus: (state, payload) => {
    state.teamsUpdated = payload
  },
  updateTeamName: (state, payload) => {
    state['teams'].filter(team => {
      return team.id === payload.teamId
    })[0].name = payload.name

  }
}
