import axios from 'axios';

export default {
  async addTeam ({commit}, payload){
    const {data} = await axios.post(`http://${process.env.VUE_APP_API_URL}/team`,{name:payload.name})
    let lol = await commit('appendTeam', data)
    return lol

  },
  async addPokemon ({commit}, payload){
    const {data} = await axios.post(`http://${process.env.VUE_APP_API_URL}/addPoke/${payload.id}`)
    commit('appendPokemon', {team_id:payload.id,pokemon:data.pokemon})
    return data.pokemon
  },
  updateTeams: ({commit}, payload) => {
    console.log("Update action received, calling mutator...")
    commit('updateFromCache',payload)
  },
  setStatusUpdateTeam: ({commit}, payload) =>{
    commit('setUpdateStatus',payload)
  },
  async changeTeamName({commit}, payload) {
    const {data} = await axios.put(`http://${process.env.VUE_APP_API_URL}/team/${payload.teamId}`,{newName:payload.name})
    commit('updateTeamName', payload)
  }
}
